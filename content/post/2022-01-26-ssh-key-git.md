---
title: How to configure a SSH key for Gitlab and Github
date: 2022-01-26
---

1 min read

This is my first post, how exciting!

# Generate your key

In case you don't have your key pair, let's generate it.
If you already have your key, skip to the [next step](#add-key-to-Gitlab)

1. Generate your ssh key with Ed25519 algorithm.
Replace the email with your own.
You will be asked to enter a passphrase, it is highly recommended to use a passphrase.
```shell
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/git_ssh_key -C "guilhermeboczkovski@gmail.com"
```
3. Add Your Key to the SSH Agent.
You do that so you don't need to write your passphrase everytime you use the key.
```shell
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/git_ssh_key
```

# Add key to Gitlab

1. Copy your public key
```shell
cat ~/.ssh/git_ssh_key.pub
```
2. Access this link: https://git.bry.com.br/-/profile/keys
3. Paste the public key on the field "Key" and then click "Add Key"

# Add key to Github

1. Copy your public key
```shell
cat ~/.ssh/git_ssh_key.pub
```
2. Access this link: https://github.com/settings/keys
3. Click at "New SSH key"
4. Paste the public key on the field "Key" and then click "Add SSH key"
5. You may be asked to confirm your GitHub password

> Source: <br> 
> https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54 <br> 
> https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account <br> 
> https://docs.github.com/en/authentication/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account <br> 